#include <map>
#include <string>
#include <tuple>
#include <ros/ros.h>

struct module_frame{
    float plate_pos_x;
    float plate_pos_y;

}

class CoordinatesTransformer {
    public:
        CoordinatesTransformer();
        ~CoordinatesTransformer();
        void setGlobalRobotOffset(float, float);
        void addModule(std::string, float, float);
        void removeModule(std::string);
        void removeModule(float, float);
        void transformGlobalFrameToRobotFrame(gemoetry_msgs::Pose, geometry_msgs::Pose);
        void transformModuelFrameToRobotFrame(gemoetry_msgs::Pose, geometry_msgs::Pose);

    private:
        float global_robot_offset_x;
        float global_robot_offset_y;
        std::map<std::string, std::tuple<float, float>> modules;
};