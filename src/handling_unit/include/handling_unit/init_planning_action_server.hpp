#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <handling_unit/Init_planningAction.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include "handling_unit/environment_builder.hpp"
#include <nlohmann/json.hpp>
#include <algorithm>
#include <string>
#include <tf2_ros/static_transform_broadcaster.h>
#include <geometry_msgs/TransformStamped.h>


class InitPlanningActionServer
{
    private:
        std::string frame_id = "";
        moveit::planning_interface::PlanningSceneInterface* psi_ptr;


    protected:
        ros::NodeHandle nh_;
        actionlib::SimpleActionServer<handling_unit::Init_planningAction> as_; // NodeHandle instance must be created before this line. Otherwise strange error occurs.
        std::string action_name_;
        // create messages that are used to published feedback/result
        handling_unit::Init_planningFeedback feedback_;
        handling_unit::Init_planningResult result_;

    public:
        InitPlanningActionServer(std::string name, std::string frame_id, moveit::planning_interface::PlanningSceneInterface*);

        ~InitPlanningActionServer(void);

        void executeCB(const handling_unit::Init_planningGoalConstPtr &goal);

};