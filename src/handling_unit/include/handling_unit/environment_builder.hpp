#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit_msgs/CollisionObject.h>
#include "geometric_shapes/shapes.h"
#include "geometric_shapes/mesh_operations.h"
#include "geometric_shapes/shape_operations.h"
//#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <math.h>
#include <tf2/LinearMath/Quaternion.h>
#include <ros/ros.h>
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/static_transform_broadcaster.h>

class EnvironmentBuilder {
    
    public:
        EnvironmentBuilder(moveit::planning_interface::PlanningSceneInterface*);
        ~EnvironmentBuilder();

        void addBasePlate(std::string, std::string, float, float);
        void addMachineMesh(std::string, std::string, std::string, float, std::string, bool);
        void addMachineMesh(std::string, std::string, float, float, float, std::string, bool);
        void addTable(std::string, std::string, float, float);

    private:    
        moveit::planning_interface::PlanningSceneInterface* planning_scene_interface = nullptr;
        const tf2_ros::TransformListener* tf_listener_ptr;
        tf2_ros::Buffer* tf_buffer_ptr;
        
        geometry_msgs::TransformStamped getTransform(std::string, std::string);
        void addCollisionProtectionVolumina(std::string);
};