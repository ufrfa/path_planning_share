#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <handling_unit/Planning_requestAction.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <nlohmann/json.hpp>
#include <algorithm>
#include <moveit/moveit_cpp/moveit_cpp.h>
#include <moveit/moveit_cpp/planning_component.h>
#include <moveit_visual_tools/moveit_visual_tools.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/planning_scene/planning_scene.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit_visual_tools/moveit_visual_tools.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit/transforms/transforms.h>
#include <moveit/robot_state/conversions.h>
#include <moveit_msgs/RobotState.h>
#include <moveit_msgs/RobotTrajectory.h>
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include "tf2_ros/transform_listener.h"
#include <iostream>
#include <fstream>
#include <math.h>
#include <cmath>
#include "sole/sole.hpp"
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <Eigen/Geometry>
//#include <moveit/moveit_cpp/planning_response.h>

class PlanningRequestActionServer
{
    private:
        moveit_cpp::MoveItCppPtr moveit_cpp_ptr = nullptr;
        std::shared_ptr<std::vector<double>> start_joint_values;
        ros::NodeHandle* nh_ptr = nullptr;
        std::shared_ptr<const tf2_ros::TransformListener> tf_listener_ptr;
        std::shared_ptr<tf2_ros::Buffer> tf_buffer_ptr;
        const moveit::core::JointModelGroup* joint_model_group;

        void setResult(bool, std::string, std::string);


    protected:
        ros::NodeHandle nh_;
        actionlib::SimpleActionServer<handling_unit::Planning_requestAction> as_; // NodeHandle instance must be created before this line. Otherwise strange error occurs.
        std::string action_name_;
        // create messages that are used to published feedback/result
        handling_unit::Planning_requestFeedback feedback_;
        handling_unit::Planning_requestResult result_;

        static double weightedDistance(const planning_interface::MotionPlanResponse&);
        static planning_interface::MotionPlanResponse getShortestWeightedSolution(const std::vector<planning_interface::MotionPlanResponse>&);
        static bool stoppingCriterion(moveit_cpp::PlanningComponent::PlanSolutions const&, moveit_cpp::PlanningComponent::MultiPipelinePlanRequestParameters const&);

        planning_interface::MotionPlanResponse planMultipleGoals(std::vector<geometry_msgs::PoseStamped>);


    public:
        PlanningRequestActionServer(std::string name, moveit_cpp::MoveItCppPtr, ros::NodeHandle*);

        ~PlanningRequestActionServer(void);

        void executeCB(const handling_unit::Planning_requestGoalConstPtr &goal);

};