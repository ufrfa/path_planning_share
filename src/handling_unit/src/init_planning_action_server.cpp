#include "handling_unit/init_planning_action_server.hpp"

InitPlanningActionServer::InitPlanningActionServer(std::string name, std::string frame_id, moveit::planning_interface::PlanningSceneInterface* psi_ptr) :
            as_(nh_, name, boost::bind(&InitPlanningActionServer::executeCB, this, _1), false),
            action_name_(name){
                as_.start();
                this->frame_id = frame_id;
                this->psi_ptr = psi_ptr;
            }

InitPlanningActionServer::~InitPlanningActionServer(void){
}

void InitPlanningActionServer::executeCB(const handling_unit::Init_planningGoalConstPtr &goal){
    bool success = false;
    std::string error_info = "";
    nlohmann::json result_json;
    if(as_.isPreemptRequested() || !ros::ok()){
        ROS_INFO("%s: Preempted", action_name_.c_str());
        error_info = "Action Preempted!";
        success = false;
        result_json["success"] = success;
        result_json["error_info"] = error_info;
        result_.result = result_json.dump();
        as_.setPreempted(result_);
        return;
    }
    feedback_.feedback = "Starting initialization.";
    as_.publishFeedback(feedback_);
    if(this->frame_id == ""){
        success = false;
        error_info = "Did not receive planning frame id.";
        result_json["success"] = success;
        result_json["error_info"] = error_info;
        result_.result = result_json.dump();
        as_.setAborted(result_);
        return;
    }
    std::string goal_msg = goal->configuration;
    nlohmann::json goal_json;
    try{
        goal_json = nlohmann::json::parse(goal_msg);
    } catch (nlohmann::json::parse_error& ex) {
        success = false;
        error_info = "Received goal is not valid JSON and could not be parsed to string.\nError at position: " + std::to_string(ex.byte) + "\n" + ex.what();
        result_json["success"] = success;
        result_json["error_info"] = error_info;
        result_.result = result_json.dump();
        as_.setAborted(result_);
        return;
    }
    static tf2_ros::StaticTransformBroadcaster static_broadcaster;
    geometry_msgs::TransformStamped static_transformStamped;
    static_transformStamped.header.stamp = ros::Time::now();
    static_transformStamped.header.frame_id = "base_link";
    static_transformStamped.child_frame_id = "global";
    tf2::Quaternion quat;
    quat.setRPY(0, 0, 0);
    static_transformStamped.transform.rotation.x = quat.x();
    static_transformStamped.transform.rotation.y = quat.y();
    static_transformStamped.transform.rotation.z = quat.z();
    static_transformStamped.transform.rotation.w = quat.w();
    EnvironmentBuilder env_builder = EnvironmentBuilder(this->psi_ptr);
    std::string robot_offset_x = goal_json["robot"]["position"]["x"].dump();
    std::string robot_offset_y = goal_json["robot"]["position"]["y"].dump();
    static_transformStamped.transform.translation.x = std::stof(robot_offset_x) + 0.25;
    static_transformStamped.transform.translation.y = std::stof(robot_offset_y) + 0.25;
    static_broadcaster.sendTransform(static_transformStamped);
    //env_builder.setGlobalRobotOffset();
    //env_builder.setGlobalRobotOffset(0.25 + std::stof(robot_offset_x), 0.25 + std::stof(robot_offset_y));
    env_builder.addTable(this->frame_id, "table0", 0.0, 0.0);
    std::unique_ptr<std::string> table_id = std::make_unique<std::string>("");
    std::unique_ptr<std::string> pos_x = std::make_unique<std::string>("");
    std::unique_ptr<std::string> pos_y = std::make_unique<std::string>("");
    for(auto& table : goal_json["table_config"].items()){
        *table_id = table.value()["id"].dump();
        table_id->erase(remove(table_id->begin(), table_id->end(), '\"'), table_id->end());
        ROS_INFO("Table: %s", table_id->c_str());
        *pos_x = table.value()["position"]["x"].dump();
        pos_x->erase(remove(pos_x->begin(), pos_x->end(), '\"'), pos_x->end());
        ROS_INFO("Pos X: %s", pos_x->c_str());
        *pos_y = table.value()["position"]["y"].dump();
        pos_y->erase(remove(pos_y->begin(), pos_y->end(), '\"'), pos_y->end());
        ROS_INFO("Pos X: %s", pos_y->c_str());
        if (*table_id == "" || *pos_x == "" || *pos_y == ""){
            success = false;
            error_info = "Received invalid table configuration";
            result_json["success"] = success;
            result_json["error_info"] = error_info;
            result_.result = result_json.dump();
            as_.setAborted(result_);
            return;
        }
        env_builder.addTable(this->frame_id, *table_id, std::stof(*pos_x), std::stof(*pos_y));
    }
    std::unique_ptr<std::string> module_id = std::make_unique<std::string>("");
    std::unique_ptr<std::string> mesh = std::make_unique<std::string>("");
    std::unique_ptr<std::string> plate_pos_x = std::make_unique<std::string>("");
    std::unique_ptr<std::string> plate_pos_y = std::make_unique<std::string>("");
    std::unique_ptr<std::string> orientation_str = std::make_unique<std::string>("");
    std::unique_ptr<float> orientation = std::make_unique<float>();
    for(auto& module : goal_json["modules"].items()){
        *module_id = module.value()["module_id"].dump();
        module_id->erase(remove(module_id->begin(), module_id->end(), '\"'), module_id->end());
        *mesh = module.value()["3d_mesh"].dump();
        mesh->erase(remove(mesh->begin(), mesh->end(), '\"'), mesh->end());
        *plate_pos_x = module.value()["pose"]["plate"]["x"].dump();
        plate_pos_x->erase(remove(plate_pos_x->begin(), plate_pos_x->end(), '\"'), plate_pos_x->end());
        *plate_pos_y = module.value()["pose"]["plate"]["y"].dump();
        plate_pos_y->erase(remove(plate_pos_y->begin(), plate_pos_y->end(), '\"'), plate_pos_y->end());
        *orientation_str = module.value()["pose"]["orientation"].dump();
        orientation_str->erase(remove(orientation_str->begin(), orientation_str->end(), '\"'), orientation_str->end());
        *orientation = std::stof(*orientation_str);
        if (*module_id == "" || *plate_pos_x == "" || *plate_pos_y == "" || *orientation_str == ""){ //|| *mesh == ""
            success = false;
            error_info = "Received invalid module configuration.";
            result_json["success"] = success;
            result_json["error_info"] = error_info;
            result_.result = result_json.dump();
            as_.setAborted(result_);
            return;
        }
        env_builder.addMachineMesh(this->frame_id, *module_id, std::stof(*plate_pos_x), std::stof(*plate_pos_y), *orientation, *mesh, true);
    }
    success = true;
    error_info = "";
    result_json["success"] = success;
    result_json["error_info"] = error_info;
    result_.result = result_json.dump();
    as_.setSucceeded(result_);
}