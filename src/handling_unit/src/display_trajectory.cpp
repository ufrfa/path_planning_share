/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, SRI International
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of SRI International nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Sachin Chitta, Dave Coleman, Mike Lautman */
#include "geometric_shapes/shapes.h"
#include "geometric_shapes/mesh_operations.h"
#include "geometric_shapes/shape_operations.h"
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/moveit_cpp/moveit_cpp.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <moveit_visual_tools/moveit_visual_tools.h>
#include "handling_unit/environment_builder.hpp"
#include <eigen_conversions/eigen_msg.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <tf_conversions/tf_eigen.h>
#include <ros/ros.h>
//#include <control_msgs/FollowJointTrajectoryAction.h>
#include <actionlib/client/simple_action_client.h>
#include <visualization_msgs/MarkerArray.h>
#include <vector>
#include <boost/math/constants/constants.hpp>
#include <map>
#include "handling_unit/StoredRobotTrajectory.h"
#include "handling_unit/StoredRobotTrajectoryMetrics.h"
#include <rosbag/bag.h>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include <sstream>
#include <string>
#include <stdlib.h>
#include <moveit/moveit_cpp/planning_component.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <geometry_msgs/TransformStamped.h>
#include <moveit/transforms/transforms.h>
#include <moveit/robot_state/conversions.h>
#include <moveit_msgs/RobotState.h>
#include <moveit_msgs/RobotTrajectory.h>
#include <cmath>
#include <nlohmann/json.hpp>
#include "std_msgs/String.h"
#include <boost/bind.hpp>



void trajectoriesCallback(std::shared_ptr<moveit_cpp::PlanningComponent> planning_component, const moveit::core::JointModelGroup* joint_model_group_ptr, ros::NodeHandle& node_handle, std::shared_ptr<moveit_cpp::MoveItCpp> moveit_cpp_ptr, const std_msgs::String::ConstPtr& msg)
  {
    ros::Publisher display_publisher = node_handle.advertise<moveit_msgs::DisplayTrajectory>("/move_group/display_planned_path", 1, true);
    std::ifstream ifs;
    //ifs.open ("test/trajectories/EST_move_to_storage_2.json", std::ifstream::in);
    ifs.open (msg->data, std::ifstream::in);
    std::string json_string = "";
    char c = ifs.get();
    while (ifs.good()) {
      json_string += c;
      //std::cout << c;
      c = ifs.get();
    }
    nlohmann::json trajectory_json = nlohmann::json::parse(json_string);
    std::vector<std::string> joint_names_from_json; 
    joint_names_from_json.push_back("shoulder_pan_joint");
    joint_names_from_json.push_back("shoulder_lift_joint");
    joint_names_from_json.push_back("elbow_joint");
    joint_names_from_json.push_back("wrist_1_joint");
    joint_names_from_json.push_back("wrist_2_joint");
    joint_names_from_json.push_back("wrist_3_joint");
    std::string* value_string = new std::string("");
    double* value = new double(0.0);
    std::vector<double> start_joint_values;
    if(trajectory_json["start"].dump() != "{}"){
      for(std::string joint_name : joint_names_from_json){
          *value_string = trajectory_json["start"]["state"][joint_name].dump();
          value_string->erase(remove(value_string->begin(), value_string->end(), '\"'), value_string->end());
          *value = std::stod(*value_string);
          start_joint_values.push_back(*value);
      }       
    }
    auto robot_start_state = planning_component->getStartState();
    robot_start_state->setJointGroupPositions(joint_model_group_ptr, start_joint_values);
    auto robot_model_ptr = moveit_cpp_ptr->getRobotModel();
    robot_trajectory::RobotTrajectory robot_traj = robot_trajectory::RobotTrajectory (robot_model_ptr, joint_model_group_ptr);
    robot_traj.addSuffixWayPoint(*robot_start_state, 1);
    if(trajectory_json["waypoints"].dump() != "{}"){
      std::vector<double> joint_values;
      for (auto& element : trajectory_json["waypoints"].items())
      {
          joint_values.clear();
          auto waypoint_state = planning_component->getStartState();
          for (auto& waypoint : element.value().items()){
            for(auto& joint_value : waypoint.value()){
              *value = joint_value;
              joint_values.push_back(*value);
            }
          }
          waypoint_state->setJointGroupPositions(joint_model_group_ptr, joint_values);
          robot_traj.addSuffixWayPoint(*waypoint_state, 1);
      }
    }
    delete value_string;
    delete value;
    moveit_msgs::RobotTrajectory robot_traj_msg;
    moveit_msgs::RobotState robot_state_msg;
    robot_traj.getRobotTrajectoryMsg(robot_traj_msg);
    moveit_msgs::DisplayTrajectory display_trajectory;
    moveit::core::robotStateToRobotStateMsg (*robot_start_state, robot_state_msg, true);
    display_trajectory.trajectory_start = robot_state_msg;
    display_trajectory.trajectory.push_back(robot_traj_msg);
    sleep(2.0);
    display_publisher.publish(display_trajectory);
    sleep(2.0);
  }


int main(int argc, char** argv)
{
  /*
  std::string env_var_name = "ROS_MASTER_URI";
  std::string env_var_value = "http://localhost:11313/"; //http://localhost:11311
  setenv(env_var_name.c_str(), env_var_value.c_str(), 1);
  */

  ros::init(argc, argv, "display_trajectory");
  ros::NodeHandle node_handle("/display_trajectory");
  ros::AsyncSpinner spinner(3);
  spinner.start();
  static const std::string PLANNING_GROUP = "arm"; //panda_arm
  ros::Duration(1.0).sleep();
  auto moveit_cpp_ptr = std::make_shared<moveit_cpp::MoveItCpp>(node_handle);
  ROS_INFO_NAMED("tutorial", "1");
  auto planning_component = std::make_shared<moveit_cpp::PlanningComponent>("arm", moveit_cpp_ptr);
  auto robot_model_ptr = moveit_cpp_ptr->getRobotModel();
  auto robot_start_state = planning_component->getStartState();
  auto robot_target_state = planning_component->getStartState();
  auto joint_model_group_ptr = robot_model_ptr->getJointModelGroup(PLANNING_GROUP);
  ros::Subscriber sub = node_handle.subscribe<std_msgs::String>("/trajectories", 1000, boost::bind(&trajectoriesCallback, planning_component, joint_model_group_ptr, std::ref(node_handle), moveit_cpp_ptr, _1));
  
  ROS_INFO_NAMED("tutorial", "2");
  moveit::planning_interface::MoveGroupInterface move_group_interface(PLANNING_GROUP);
  auto psm = moveit_cpp_ptr->getPlanningSceneMonitorNonConst();
  psm->startSceneMonitor();
  psm->startWorldGeometryMonitor();
  psm->startStateMonitor();
  psm->startPublishingPlanningScene(planning_scene_monitor::PlanningSceneMonitor::SceneUpdateType::UPDATE_SCENE);
  psm->providePlanningSceneService();
  ROS_INFO_NAMED("tutorial", "3");
  moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
  namespace rvt = rviz_visual_tools;
  moveit_visual_tools::MoveItVisualTools visual_tools("base_link"); //panda_link0
  visual_tools.deleteAllMarkers();
  visual_tools.loadRemoteControl();
  Eigen::Isometry3d text_pose = Eigen::Isometry3d::Identity();
  text_pose.translation().z() = 1.0;
  visual_tools.publishText(text_pose, "MoveGroupInterface Demo", rvt::WHITE, rvt::XLARGE);
  visual_tools.trigger();
  ROS_INFO_NAMED("tutorial", "Planning frame: %s", move_group_interface.getPlanningFrame().c_str());
  ROS_INFO_NAMED("tutorial", "End effector link: %s", move_group_interface.getEndEffectorLink().c_str());
  ROS_INFO_NAMED("tutorial", "Available Planning Groups:");
  std::copy(move_group_interface.getJointModelGroupNames().begin(),
            move_group_interface.getJointModelGroupNames().end(), std::ostream_iterator<std::string>(std::cout, ", "));

  //Add environment

  static tf2_ros::StaticTransformBroadcaster static_broadcaster;
  geometry_msgs::TransformStamped static_transformStamped;
  static_transformStamped.header.stamp = ros::Time::now();
  static_transformStamped.header.frame_id = "base_link";
  static_transformStamped.child_frame_id = "global";
  tf2::Quaternion quat;
  quat.setRPY(0, 0, 0);
  static_transformStamped.transform.rotation.x = quat.x();
  static_transformStamped.transform.rotation.y = quat.y();
  static_transformStamped.transform.rotation.z = quat.z();
  static_transformStamped.transform.rotation.w = quat.w();
  static_transformStamped.transform.translation.x = 0.115;
  static_transformStamped.transform.translation.y = 0.115;
  static_broadcaster.sendTransform(static_transformStamped);
  EnvironmentBuilder env_builder = EnvironmentBuilder(&planning_scene_interface);
  env_builder.addBasePlate(move_group_interface.getPlanningFrame(), "base_plate_handling_unit_A", 0.0, 0.0);
  env_builder.addBasePlate(move_group_interface.getPlanningFrame(), "base_plate_storage_A", 1.0, 0.0);
  env_builder.addBasePlate(move_group_interface.getPlanningFrame(), "base_plate_auspresseinheit_A", 0.0, 1.0);
  env_builder.addBasePlate(move_group_interface.getPlanningFrame(), "base_plate_magneteinheit_A", 1.0, 1.0);

  env_builder.addBasePlate(move_group_interface.getPlanningFrame(), "base_plate_handling_unit_B", 0.0, -1.0);
  env_builder.addBasePlate(move_group_interface.getPlanningFrame(), "base_plate_storage_B", 1.0, -1.0);
  env_builder.addBasePlate(move_group_interface.getPlanningFrame(), "base_plate_auspresseinheit_B", 0.0, -2.0);
  env_builder.addBasePlate(move_group_interface.getPlanningFrame(), "base_plate_magneteinheit_B", 1.0, -2.0);

  env_builder.addBasePlate(move_group_interface.getPlanningFrame(), "base_plate_handling_unit_C", -1.0, -1.0);
  env_builder.addBasePlate(move_group_interface.getPlanningFrame(), "base_plate_storage_C", -2.0, -1.0);
  env_builder.addBasePlate(move_group_interface.getPlanningFrame(), "base_plate_auspresseinheit_C", -2.0, -2.0);
  env_builder.addBasePlate(move_group_interface.getPlanningFrame(), "base_plate_magneteinheit_C", -1.0, -2.0);

  env_builder.addBasePlate(move_group_interface.getPlanningFrame(), "base_plate_handling_unit_D", -1.0, 0.0);
  env_builder.addBasePlate(move_group_interface.getPlanningFrame(), "base_plate_storage_D", -2.0, 0.0);
  env_builder.addBasePlate(move_group_interface.getPlanningFrame(), "base_plate_auspresseinheit_D", -2.0, 1.0);
  env_builder.addBasePlate(move_group_interface.getPlanningFrame(), "base_plate_magneteinheit_D", -1.0, 1.0);

  env_builder.addMachineMesh(move_group_interface.getPlanningFrame(), "auspresseinheit_A", "base_plate_auspresseinheit_A", 0.0, "/home/loris/test_ws/src/descriptions/units/auspresseinheit.dae", true);
  env_builder.addMachineMesh(move_group_interface.getPlanningFrame(), "magneteinheit_A", "base_plate_magneteinheit_A", 0.0, "/home/loris/test_ws/src/descriptions/units/magneteinheit.dae", true);

  env_builder.addMachineMesh(move_group_interface.getPlanningFrame(), "auspresseinheit_B", "base_plate_auspresseinheit_B", 2.0, "/home/loris/test_ws/src/descriptions/units/auspresseinheit.dae", true);
  env_builder.addMachineMesh(move_group_interface.getPlanningFrame(), "magneteinheit_B", "base_plate_magneteinheit_B", 2.0, "/home/loris/test_ws/src/descriptions/units/magneteinheit.dae", true);

  env_builder.addMachineMesh(move_group_interface.getPlanningFrame(), "auspresseinheit_C", "base_plate_auspresseinheit_C", 2.0, "/home/loris/test_ws/src/descriptions/units/auspresseinheit.dae", true);
  env_builder.addMachineMesh(move_group_interface.getPlanningFrame(), "magneteinheit_C", "base_plate_magneteinheit_C", 2.0, "/home/loris/test_ws/src/descriptions/units/magneteinheit.dae", true);

  env_builder.addMachineMesh(move_group_interface.getPlanningFrame(), "auspresseinheit_D", "base_plate_auspresseinheit_D", 0.0, "/home/loris/test_ws/src/descriptions/units/auspresseinheit.dae", true);
  env_builder.addMachineMesh(move_group_interface.getPlanningFrame(), "magneteinheit_D", "base_plate_magneteinheit_D", 0.0, "/home/loris/test_ws/src/descriptions/units/magneteinheit.dae", true);

  ros::waitForShutdown();
  return 0;
}



