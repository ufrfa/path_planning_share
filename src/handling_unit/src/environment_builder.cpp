#include "handling_unit/environment_builder.hpp"

EnvironmentBuilder::EnvironmentBuilder(moveit::planning_interface::PlanningSceneInterface* planning_scene_interface){
    this->planning_scene_interface = planning_scene_interface;
    this->tf_buffer_ptr = new tf2_ros::Buffer();
    this->tf_listener_ptr = new tf2_ros::TransformListener(*(this->tf_buffer_ptr));
}


EnvironmentBuilder::~EnvironmentBuilder(){
    delete this->tf_listener_ptr;
    delete this->tf_buffer_ptr;
}


geometry_msgs::TransformStamped EnvironmentBuilder::getTransform(std::string frame_id, std::string child_frame_id){
    ROS_INFO_NAMED("EnvironmentBuilder", "LOOK FOR TRANSFORM");
    geometry_msgs::TransformStamped transformStamped;
    try{
        transformStamped = this->tf_buffer_ptr->lookupTransform(frame_id, child_frame_id,
                                    ros::Time(0), ros::Duration(2.0));
    }
    catch (tf2::TransformException &ex) {
        ROS_INFO_NAMED("EnvironmentBuilder", "COULD NOT ACCESS TRANSFORMATION %s --> %s", frame_id.c_str(), child_frame_id.c_str());
        ROS_WARN("%s",ex.what());
        ros::Duration(1.0).sleep();
    }
    return transformStamped;
}

void EnvironmentBuilder::addBasePlate(std::string frame_id, std::string id, float x, float y){
    ROS_INFO_NAMED("EnvironmentBuilder", "ADD BASE PLATE");
    //ROS_INFO_NAMED("EnvironmentBuilder", "FRAME ID: %s", frame_id.c_str());
    moveit_msgs::CollisionObject base_plate;
    base_plate.header.frame_id = frame_id;
    base_plate.id = id;
    ROS_INFO_NAMED("EnvironmentBuilder", "BASE PLATE ID: %s", base_plate.id.c_str());
    shape_msgs::SolidPrimitive base_plate_primitive;
    base_plate_primitive.type = base_plate_primitive.BOX;
    base_plate_primitive.dimensions.resize(3);
    base_plate_primitive.dimensions[base_plate_primitive.BOX_X] = 0.5;
    base_plate_primitive.dimensions[base_plate_primitive.BOX_Y] = 0.5;
    base_plate_primitive.dimensions[base_plate_primitive.BOX_Z] = 1.02;
    geometry_msgs::TransformStamped transform_base_link_global = this->getTransform("base_link", "global");
    geometry_msgs::Pose base_plate_pose;
    base_plate_pose.orientation.w = 1.0;
    base_plate_pose.position.x = x*0.5 + transform_base_link_global.transform.translation.x;
    base_plate_pose.position.y = y*0.5 + transform_base_link_global.transform.translation.y;
    base_plate_pose.position.z = -0.5101;
    ROS_INFO_NAMED("EnvironmentBuilder", "BASE PLATE POSE X: %f", base_plate_pose.position.x);
    ROS_INFO_NAMED("EnvironmentBuilder", "BASE PLATE POSE Y: %f", base_plate_pose.position.y);
    base_plate.primitives.push_back(base_plate_primitive);
    base_plate.primitive_poses.push_back(base_plate_pose);
    base_plate.operation = base_plate.ADD;
    std::vector<moveit_msgs::CollisionObject> base_plates;
    base_plates.push_back(base_plate);
    this->planning_scene_interface->addCollisionObjects(base_plates);
    ROS_INFO_NAMED("EnvironmentBuilder", "ADDED BASE PLATE");
    /*
    ROS_INFO_NAMED("EnvironmentBuilder", "ALL OBJECTS");
    ROS_INFO_NAMED("EnvironmentBuilder", "Planning Scene Interface Adress: %p", this->planning_scene_interface);
    std::map< std::string, moveit_msgs::CollisionObject > objects = this->planning_scene_interface->getObjects();
    if(objects.empty()){
        ROS_INFO_NAMED("EnvironmentBuilder", "NO OBJECTS");
    }
    for(std::map< std::string, moveit_msgs::CollisionObject >::iterator objects_itr = objects.begin(); objects_itr != objects.end(); ++objects_itr){
        ROS_INFO_NAMED("tutorial", "Object: %s", (objects_itr->first).c_str());
    }
    */
}

void EnvironmentBuilder::addTable(std::string frame_id, std::string id, float x, float y){
    float plate_pos_x = 0;
    float plate_pos_y = 0;
    if (x != 0.0){
        plate_pos_x = 2*x;
    }
    if (y != 0.0){
        plate_pos_y = 2*y;
    }
    std::string plate_id = std::to_string(plate_pos_x) + "," + std::to_string(plate_pos_y);
    this->addBasePlate(frame_id, plate_id, plate_pos_x, plate_pos_y);
    plate_id = std::to_string(plate_pos_x + 1.0) + "," + std::to_string(plate_pos_y);
    this->addBasePlate(frame_id, plate_id, plate_pos_x + 1.0, plate_pos_y);
    plate_id = std::to_string(plate_pos_x) + "," + std::to_string(plate_pos_y + 1.0);
    this->addBasePlate(frame_id, plate_id, plate_pos_x, plate_pos_y + 1.0);
    plate_id = std::to_string(plate_pos_x + 1.0) + "," + std::to_string(plate_pos_y + 1.0);
    this->addBasePlate(frame_id, plate_id, plate_pos_x + 1.0, plate_pos_y + 1.0);

}

void EnvironmentBuilder::addMachineMesh(std::string frame_id, std::string id, std::string base_plate_id, float orientation, std::string file_name, bool has_plate){
    ROS_INFO_NAMED("EnvironmentBuilder", "ADD MACHINE");
    ROS_INFO_NAMED("EnvironmentBuilder", "ID: %s", id.c_str());
    //ROS_INFO_NAMED("EnvironmentBuilder", "BASE PLATE ID: %s", base_plate_id.c_str());
    //ROS_INFO_NAMED("EnvironmentBuilder", "FRAME ID: %s", frame_id.c_str());
    std::vector<std::string> base_plate_ids = std::vector<std::string>();
    base_plate_ids.push_back(base_plate_id);
    /*
    ROS_INFO_NAMED("EnvironmentBuilder", "ALL OBJECTS");
    ROS_INFO_NAMED("EnvironmentBuilder", "Planning Scene Interface Adress: %p", this->planning_scene_interface);
    for(std::string object_name : this->planning_scene_interface->getKnownObjectNames()){
        ROS_INFO_NAMED("tutorial", "Object: %s", object_name.c_str());
    }
    std::map<std::string, geometry_msgs::Pose> base_plate_poses = this->planning_scene_interface->getObjectPoses(base_plate_ids);
    if(base_plate_poses.empty()) {
        ROS_INFO_NAMED("tutorial", "NO BASE PLATE WITH THAT ID FOUND");
        return;
    } else if(base_plate_poses.count(base_plate_id) <= 0 ){
        ROS_INFO_NAMED("tutorial", "NO BASE PLATE WITH THAT ID FOUND XX");
        return;
    }
    */
    std::map<std::string, geometry_msgs::Pose> base_plate_poses = this->planning_scene_interface->getObjectPoses(base_plate_ids);
    geometry_msgs::Pose base_plate_pose = base_plate_poses[base_plate_id];
    Eigen::Vector3d vectorScale(1, 1, 1);
    moveit_msgs::CollisionObject machine;
    machine.header.frame_id = frame_id;
    machine.id = id;
    shape_msgs::Mesh machine_shape_msg_mesh;
    if(file_name != ""){
        shapes::Mesh* machine_mesh = shapes::createMeshFromResource(("file://" + file_name), vectorScale);
        shapes::ShapeMsg machine_mesh_msg;
        shapes::constructMsgFromShape(machine_mesh, machine_mesh_msg);
        machine_shape_msg_mesh = boost::get<shape_msgs::Mesh>(machine_mesh_msg);
    }
    geometry_msgs::Pose machine_pose;
    tf2::Quaternion orientationQuaternion;
    orientationQuaternion.setRPY(0, 0, (M_PI/2)*(orientation+3));
    orientationQuaternion.normalize();
    machine_pose.orientation.w = orientationQuaternion.getW();
    machine_pose.orientation.z = orientationQuaternion.getZ();
    machine_pose.orientation.x = orientationQuaternion.getX();
    machine_pose.orientation.y = orientationQuaternion.getY();
    machine_pose.position.x = base_plate_pose.position.x;
    machine_pose.position.y = base_plate_pose.position.y;
    ROS_INFO_NAMED("EnvironmentBuilder", "MACHINE POSE X: %f", machine_pose.position.x);
    ROS_INFO_NAMED("EnvironmentBuilder", "MACHINE POSE Y: %f", machine_pose.position.y);
    if(has_plate) {
        machine_pose.position.z = -0.0585;
        }
    else {
        machine_pose.position.z = 0.0;
    }
    machine.meshes.push_back(machine_shape_msg_mesh);
    machine.mesh_poses.push_back(machine_pose);
    machine.operation = machine.ADD;
    std::vector<moveit_msgs::CollisionObject> machines;
    machines.push_back(machine);
    this->planning_scene_interface->addCollisionObjects(machines);
    static tf2_ros::StaticTransformBroadcaster static_broadcaster;
    geometry_msgs::TransformStamped static_transformStamped;
    static_transformStamped.header.stamp = ros::Time::now();
    static_transformStamped.header.frame_id = "base_link";
    static_transformStamped.child_frame_id = id;
    static_transformStamped.transform.translation.x = machine_pose.position.x;
    static_transformStamped.transform.translation.y = machine_pose.position.y;
    static_transformStamped.transform.translation.z = machine_pose.position.z;
    static_transformStamped.transform.rotation.x = machine_pose.orientation.x;
    static_transformStamped.transform.rotation.y = machine_pose.orientation.y;
    static_transformStamped.transform.rotation.z = machine_pose.orientation.z;
    static_transformStamped.transform.rotation.w = machine_pose.orientation.w;
    static_broadcaster.sendTransform(static_transformStamped);
    if(id != "" && (id.find("Magnetentnahmeeinheit") != std::string::npos || id.find("Ausspresseinheit") != std::string::npos)){
        this->addCollisionProtectionVolumina(id);
    }
    ROS_INFO_NAMED("EnvironmentBuilder", "ADDED MACHINE");
}

void EnvironmentBuilder::addMachineMesh(std::string frame_id, std::string id, float plate_pos_x, float plate_pos_y, float orientation, std::string file_name, bool has_plate){
    ROS_INFO_NAMED("EnvironmentBuilder", "ADD MACHINE");
    ROS_INFO_NAMED("EnvironmentBuilder", "ID: %s", id.c_str());
    Eigen::Vector3d vectorScale(1, 1, 1);
    moveit_msgs::CollisionObject machine;
    machine.header.frame_id = frame_id;
    machine.id = id;
    shape_msgs::Mesh machine_shape_msg_mesh;
    if(file_name != ""){
        shapes::Mesh* machine_mesh = shapes::createMeshFromResource(("file://" + file_name), vectorScale);
        shapes::ShapeMsg machine_mesh_msg;
        shapes::constructMsgFromShape(machine_mesh, machine_mesh_msg);
        machine_shape_msg_mesh = boost::get<shape_msgs::Mesh>(machine_mesh_msg);
        machine_shape_msg_mesh = boost::get<shape_msgs::Mesh>(machine_mesh_msg);
    }
    geometry_msgs::Pose machine_pose;
    tf2::Quaternion orientationQuaternion;
    orientationQuaternion.setRPY(0, 0, (M_PI/2)*(orientation+3));
    orientationQuaternion.normalize();
    machine_pose.orientation.w = orientationQuaternion.getW();
    machine_pose.orientation.z = orientationQuaternion.getZ();
    machine_pose.orientation.x = orientationQuaternion.getX();
    machine_pose.orientation.y = orientationQuaternion.getY();
    geometry_msgs::TransformStamped transform_base_link_global = this->getTransform("base_link", "global");
    machine_pose.position.x = 0.5*plate_pos_x + transform_base_link_global.transform.translation.x;
    machine_pose.position.y = 0.5*plate_pos_y + transform_base_link_global.transform.translation.y;
    ROS_INFO_NAMED("EnvironmentBuilder", "MACHINE POSE X: %f", machine_pose.position.x);
    ROS_INFO_NAMED("EnvironmentBuilder", "MACHINE POSE Y: %f", machine_pose.position.y);
    if(has_plate) {
        machine_pose.position.z = -0.0585;
        }
    else {
        machine_pose.position.z = 0.0;
    }
    static tf2_ros::StaticTransformBroadcaster static_broadcaster;
    geometry_msgs::TransformStamped static_transformStamped;
    static_transformStamped.header.stamp = ros::Time::now();
    static_transformStamped.header.frame_id = "base_link";
    static_transformStamped.child_frame_id = id;
    static_transformStamped.transform.translation.x = machine_pose.position.x;
    static_transformStamped.transform.translation.y = machine_pose.position.y;
    static_transformStamped.transform.translation.z = machine_pose.position.z;
    static_transformStamped.transform.rotation.x = machine_pose.orientation.x;
    static_transformStamped.transform.rotation.y = machine_pose.orientation.y;
    static_transformStamped.transform.rotation.z = machine_pose.orientation.z;
    static_transformStamped.transform.rotation.w = machine_pose.orientation.w;
    static_broadcaster.sendTransform(static_transformStamped);
    std::vector<moveit_msgs::CollisionObject> machines;
    machine.meshes.push_back(machine_shape_msg_mesh);
    machine.mesh_poses.push_back(machine_pose);
    machine.operation = machine.ADD;
    machines.push_back(machine);
    this->planning_scene_interface->addCollisionObjects(machines);
    if(id != "" && (id.find("Magnetentnahmeeinheit") != std::string::npos || id.find("Ausspresseinheit") != std::string::npos)){
        this->addCollisionProtectionVolumina(id);
    }
    ROS_INFO_NAMED("EnvironmentBuilder", "ADDED MACHINE");
}


void EnvironmentBuilder::addCollisionProtectionVolumina(std::string machine_id){
    std::vector<moveit_msgs::CollisionObject> cpvs;
    moveit_msgs::CollisionObject cpv_top;
    moveit_msgs::CollisionObject cpv_bottom;
    moveit_msgs::CollisionObject cpv_other;
    cpv_top.header.frame_id = machine_id;
    cpv_bottom.header.frame_id = machine_id;
    cpv_other.header.frame_id = machine_id;
    std::string object_id = machine_id;
    cpv_top.id = object_id.append("_cpv_top");
    object_id = machine_id;
    cpv_bottom.id = object_id.append("_cpv_bottom");
    object_id = machine_id;
    cpv_other.id = object_id.append("_cpv_other");
    shape_msgs::SolidPrimitive collision_protection_volumina_top;
    shape_msgs::SolidPrimitive collision_protection_volumina_bottom;
    shape_msgs::SolidPrimitive cpv_other_primitive;
    collision_protection_volumina_bottom.type = collision_protection_volumina_bottom.CYLINDER;
    collision_protection_volumina_top.type = collision_protection_volumina_top.CYLINDER;
    cpv_other_primitive.type = cpv_other_primitive.CYLINDER;
    geometry_msgs::Pose cpv_bottom_pose;
    geometry_msgs::Pose cpv_top_pose;
    geometry_msgs::Pose cpv_other_pose;
    if(machine_id.find("Magnetentnahmeeinheit") != std::string::npos){
        collision_protection_volumina_bottom.dimensions.resize(2);
        collision_protection_volumina_bottom.dimensions[collision_protection_volumina_bottom.CYLINDER_HEIGHT] = 0.2;
        collision_protection_volumina_bottom.dimensions[collision_protection_volumina_bottom.CYLINDER_RADIUS] = 0.3;
        collision_protection_volumina_top.dimensions.resize(2);
        collision_protection_volumina_top.dimensions[collision_protection_volumina_top.CYLINDER_HEIGHT] = 0.5;
        collision_protection_volumina_top.dimensions[collision_protection_volumina_top.CYLINDER_RADIUS] = 0.33;
        cpv_other_primitive.dimensions.resize(2);
        cpv_other_primitive.dimensions[cpv_other_primitive.CYLINDER_HEIGHT] = 0.5;
        cpv_other_primitive.dimensions[cpv_other_primitive.CYLINDER_RADIUS] = 0.09;
        cpv_other_pose.position.x = 0.06;
        cpv_other_pose.position.y = -0.16;
        cpv_other_pose.position.z = 0.25;
        cpv_bottom_pose.position.x = 0.0;
        cpv_bottom_pose.position.y = 0.0;
        cpv_bottom_pose.position.z = 0.05;
        cpv_top_pose.position.x = -0.1;
        cpv_top_pose.position.y = 0.0;
        cpv_top_pose.position.z = 0.65;
        cpv_other.primitives.push_back(cpv_other_primitive);
        cpv_other.primitive_poses.push_back(cpv_other_pose);
        cpv_other.operation = cpv_other.ADD;
        cpvs.push_back(cpv_other);
    } else if(machine_id.find("Ausspresseinheit") != std::string::npos){
        collision_protection_volumina_bottom.dimensions.resize(2);
        collision_protection_volumina_bottom.dimensions[collision_protection_volumina_bottom.CYLINDER_HEIGHT] = 0.2;
        collision_protection_volumina_bottom.dimensions[collision_protection_volumina_bottom.CYLINDER_RADIUS] = 0.3;
        collision_protection_volumina_top.dimensions.resize(2);
        collision_protection_volumina_top.dimensions[collision_protection_volumina_top.CYLINDER_HEIGHT] = 0.5;
        collision_protection_volumina_top.dimensions[collision_protection_volumina_top.CYLINDER_RADIUS] = 0.2;
        cpv_bottom_pose.position.x = 0.0;
        cpv_bottom_pose.position.y = 0.0;
        cpv_bottom_pose.position.z = 0.02;
        cpv_top_pose.position.x = -0.1;
        cpv_top_pose.position.y = 0.0;
        cpv_top_pose.position.z = 0.75;
    }
    cpv_top.primitives.push_back(collision_protection_volumina_top);
    cpv_top.primitive_poses.push_back(cpv_top_pose);
    cpv_bottom.primitives.push_back(collision_protection_volumina_bottom);
    cpv_bottom.primitive_poses.push_back(cpv_bottom_pose);
    cpv_top.operation = cpv_top.ADD;
    cpv_bottom.operation = cpv_bottom.ADD;
    cpvs.push_back(cpv_top);
    cpvs.push_back(cpv_bottom);
    this->planning_scene_interface->addCollisionObjects(cpvs);
}
