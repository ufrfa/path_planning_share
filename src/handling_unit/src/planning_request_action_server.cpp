#include "handling_unit/planning_request_action_server.hpp"


PlanningRequestActionServer::PlanningRequestActionServer(std::string name, moveit_cpp::MoveItCppPtr moveit_cpp_ptr, ros::NodeHandle* nh_ptr) :
            as_(nh_, name, boost::bind(&PlanningRequestActionServer::executeCB, this, _1), false),
            action_name_(name){
                as_.start();
                this->moveit_cpp_ptr = moveit_cpp_ptr;
                this->start_joint_values = std::make_shared<std::vector<double>>();
                this->nh_ptr = nh_ptr;
                this->tf_buffer_ptr = std::make_shared<tf2_ros::Buffer>();
                this->tf_listener_ptr = std::make_shared<tf2_ros::TransformListener>(*(this->tf_buffer_ptr));
                this->joint_model_group = this->moveit_cpp_ptr->getRobotModel()->getJointModelGroup("arm");
                ROS_INFO("Started Planning Request Action Server");
            }

PlanningRequestActionServer::~PlanningRequestActionServer(void){
}

double PlanningRequestActionServer::weightedDistance(const planning_interface::MotionPlanResponse& mpr){
    double distance = 0.0;
    std::vector<double> joint_values_1;
    std::vector<double> joint_values_2;
    std::vector<double> weights;
    weights.push_back(1.0);
    weights.push_back(1.0);
    weights.push_back(1.0);
    weights.push_back(1.0); //2.0
    weights.push_back(1.0); //2.0
    weights.push_back(1.0); //3.0
    for (std::size_t k = 1; k < mpr.trajectory_->getWayPointCount(); ++k)
    {
        joint_values_1.clear();
        joint_values_2.clear();
        mpr.trajectory_->getWayPoint(k-1).copyJointGroupPositions("arm", joint_values_1);
        mpr.trajectory_->getWayPoint(k).copyJointGroupPositions("arm", joint_values_2);
        double point_distance = 0.0;
        for(std::size_t i = 0; i < joint_values_1.size(); ++i){
            point_distance += weights[i]*pow((joint_values_2[i] - joint_values_1[i]), 2.0);
        }
        distance += point_distance;
    }
    distance = sqrt(distance);
    ROS_INFO_NAMED("tutorial", "Distance: %lf", distance);
    return distance;  
}

planning_interface::MotionPlanResponse PlanningRequestActionServer::getShortestWeightedSolution(const std::vector<planning_interface::MotionPlanResponse>& solutions){
    // Find trajectory with minimal path
    auto const shortest_solution = std::min_element(solutions.begin(), solutions.end(),
        [](const planning_interface::MotionPlanResponse& solution_a,
        const planning_interface::MotionPlanResponse& solution_b) {
            // If both solutions were successful, check which path is shorter
            if (solution_a && solution_b)
            {
                //robot_trajectory::RobotTrajectoryPtr robot_traj = mpr.trajectory_;
                    // compute path length
                return weightedDistance(solution_a) < weightedDistance(solution_b);
            }
            // If only solution a is successful, return a
            else if (solution_a)
            {
                return true;
            }
            // Else return solution b, either because it is successful or not
            return false;
        });
    return *shortest_solution;
}

bool PlanningRequestActionServer::stoppingCriterion(
    moveit_cpp::PlanningComponent::PlanSolutions const& plan_solutions,
    moveit_cpp::PlanningComponent::MultiPipelinePlanRequestParameters const& plan_request_parameters)
{
  // Read solutions that are found up to this point from a thread safe storage
  //auto const& solutions = plan_solutions.getSolutions();
  //if(!solutions.empty()){
    //Stop if any solution is found
    //return true;
  //}
  
  // use all the planning time available
  return false;
}

void PlanningRequestActionServer::setResult(bool success, std::string error_info, std::string path_to_trajectory){
    nlohmann::json result_json;
    result_json["success"] = success;
    result_json["error_info"] = error_info;
    result_json["path_to_trajectory"] = path_to_trajectory;
    result_.result = result_json.dump();
}

planning_interface::MotionPlanResponse PlanningRequestActionServer::planMultipleGoals(std::vector<geometry_msgs::PoseStamped> target_poses){
    //TODO
    struct goal_state {
        int target_id;
        moveit::core::RobotStatePtr robot_goal_state;
        std::vector<goal_state*> previous_goals;
        std::vector<goal_state*> next_goals;
    };
    
    for(auto target_pose : target_poses){
        
    }

    
}

void PlanningRequestActionServer::executeCB(const handling_unit::Planning_requestGoalConstPtr &goal){
    bool success = false;
    std::string error_info = "";
    nlohmann::json result_json;
    nlohmann::json trajectory_json;
    sole::uuid traj_id = sole::uuid1();
    trajectory_json["id"] = traj_id.str();
    if(as_.isPreemptRequested() || !ros::ok()){
        ROS_INFO("%s: Preempted", action_name_.c_str());
        success = false;
        error_info = "Action Preempted!";
        setResult(success, error_info, "");
        as_.setPreempted(result_);
        return;
    }
    feedback_.feedback = "Starting Planning.";
    as_.publishFeedback(feedback_);
    if(this->moveit_cpp_ptr == nullptr || this->nh_ptr == nullptr){
        success = false;
        error_info = "ERROR: No move group interface specified.";
        setResult(success, error_info, "");
        as_.setAborted(result_);
        return;
    }
    std::string goal_msg = goal->planning_request;
    nlohmann::json goal_json = nlohmann::json::parse(goal_msg);
    moveit::core::RobotStatePtr robot_start_state = this->moveit_cpp_ptr->getCurrentState();
    const std::vector<std::string>& joint_names = this->joint_model_group->getVariableNames();
    std::vector<std::string> joint_names_from_json; 
    joint_names_from_json.push_back("shoulder_pan_joint");
    joint_names_from_json.push_back("shoulder_lift_joint");
    joint_names_from_json.push_back("elbow_joint");
    joint_names_from_json.push_back("wrist_1_joint");
    joint_names_from_json.push_back("wrist_2_joint");
    joint_names_from_json.push_back("wrist_3_joint");
    std::vector<double> joint_value_buffer;
    if(goal_json["start_state"].dump() != "{}"){
        for(std::string joint_name : joint_names_from_json){
            try{
               joint_value_buffer.push_back((double)goal_json["start_state"][joint_name]);
            } catch(nlohmann::json::exception const& json_ex){
                success = false;
                error_info = "Error in JSON while trying to get joint value for joint " + joint_name + ".\n" + json_ex.what();
                setResult(success, error_info, "");
                as_.setAborted(result_);
                return;
            } catch(std::invalid_argument const& inval_arg_ex){
                success = false;
                error_info = "Invalid value in start_state for joint " + joint_name + ". Expected a numerical value.\n" + inval_arg_ex.what();
                setResult(success, error_info, "");
                as_.setAborted(result_);
                return;
            } catch(std::exception const& ex){
                success = false;
                error_info = "Error while processing joint value for joint " + joint_name + ".\n" + ex.what();
                setResult(success, error_info, "");
                as_.setAborted(result_);
                return;
            }
        }
        this->start_joint_values->clear();
        for(double value : joint_value_buffer){
            this->start_joint_values->push_back(value);
        }       
    }
    for (std::size_t i = 0; i < joint_names_from_json.size(); ++i)
    {
        ROS_INFO("Joint %s: %lf", joint_names_from_json[i].c_str(), (*(this->start_joint_values))[i]);
    }
    if(goal_json["gripper_open"]){
        //lower limit: 0.0 --> 3cm open at 0.5425
        const double value = 0.5425;
        robot_start_state->setJointPositions(this->moveit_cpp_ptr->getRobotModel()->getJointModelGroup("gripper")->getJointModel("finger_joint"), &value);
        ROS_INFO_NAMED("planning_server", "Gripper OPEN");
    } else{
        if(goal_json["object_gripped"]){
            success = false;
                error_info = "Error in JSON: can not grip an object with closed gripper.\n Parameter \'object_gripped\' can not be true while parameter \'gripper_open\' is false.\n";
                setResult(success, error_info, "");
                as_.setAborted(result_);
                return;
        }
        //upper limit: 0.7 (contact at 0.7)
        const double value = 0.69;
        robot_start_state->setJointPositions(this->moveit_cpp_ptr->getRobotModel()->getJointModelGroup("gripper")->getJointModel("finger_joint"), &value);
        ROS_INFO_NAMED("planning_server", "Gripper CLOSED");
    }
    if(goal_json["object_gripped"]){
        const std::vector<std::string> touch_links = {"left_inner_finger_pad", "right_inner_finger_pad"};
        Eigen::Transform<double, 3, Eigen::Isometry> t;
        t = Eigen::AngleAxis<double>(0.0, Eigen::Vector3d::UnitZ());
        t.rotate(Eigen::AngleAxis<double>(0.0, Eigen::Vector3d::UnitX()));
        t.rotate(Eigen::AngleAxis<double>(0.0, Eigen::Vector3d::UnitY()));
        t.translate(Eigen::Vector3d(0.02, 0.0, 0.0));
        Eigen::Isometry3d pose;
        pose.translation() = t.translation();
        pose.linear() = t.rotation();
        t.setIdentity();
        Eigen::Isometry3d shape_pose;
        shape_pose.translation() = t.translation();
        shape_pose.linear() = t.rotation();
        //shapes::Box object(0.09, 0.03, 0.05);
        shapes::ShapeConstPtr object = std::make_shared<shapes::Box>(0.09, 0.03, 0.05);
        const std::vector<shapes::ShapeConstPtr> shapes = {object};
        EigenSTL::vector_Isometry3d shape_poses = {shape_pose}; 
        robot_start_state->attachBody(
            "poltopf_long",
            pose,
            shapes,
            shape_poses,
            touch_links,
            "gripper_tcp"
	    );
    }
    robot_start_state->setJointGroupPositions(joint_model_group, *(this->start_joint_values));
    auto planning_component = std::make_shared<moveit_cpp::PlanningComponent>("arm", this->moveit_cpp_ptr);
    planning_component->setStartState(*robot_start_state);
    
    if(goal_json["global_pose_target"].dump() != "{}"){
        geometry_msgs::PoseStamped target_pose;
        try{
            target_pose.header.frame_id = "global";
            target_pose.pose.position.x = (double)goal_json["global_pose_target"]["position"]["x"];
            trajectory_json["goal"]["global_pose"]["position"]["x"] = (double)goal_json["global_pose_target"]["position"]["x"];
            ROS_INFO_NAMED("planning_server", "Target position x: %f", target_pose.pose.position.x);
            target_pose.pose.position.y = (double)goal_json["global_pose_target"]["position"]["y"];
            trajectory_json["goal"]["global_pose"]["position"]["y"] = (double)goal_json["global_pose_target"]["position"]["y"];
            ROS_INFO_NAMED("planning_server", "Target position y: %f", target_pose.pose.position.y);
            target_pose.pose.position.z = (double)goal_json["global_pose_target"]["position"]["z"];
            trajectory_json["goal"]["global_pose"]["position"]["z"] = (double)goal_json["global_pose_target"]["position"]["z"];
            ROS_INFO_NAMED("planning_server", "Target position z: %f", target_pose.pose.position.z);
            target_pose.pose.orientation.x = (double)goal_json["global_pose_target"]["orientation"]["x"];
            trajectory_json["goal"]["global_pose"]["orientation"]["x"] = (double)goal_json["global_pose_target"]["orientation"]["x"];
            ROS_INFO_NAMED("planning_server", "Target orientation x: %f", target_pose.pose.orientation.x);
            target_pose.pose.orientation.y = (double)goal_json["global_pose_target"]["orientation"]["y"];
            trajectory_json["goal"]["global_pose"]["orientation"]["y"] = (double)goal_json["global_pose_target"]["orientation"]["y"];
            ROS_INFO_NAMED("planning_server", "Target orientation y: %f", target_pose.pose.orientation.y);
            target_pose.pose.orientation.z = (double)goal_json["global_pose_target"]["orientation"]["z"];
            trajectory_json["goal"]["global_pose"]["orientation"]["z"] = (double)goal_json["global_pose_target"]["orientation"]["z"];
            ROS_INFO_NAMED("planning_server", "Target orientation z: %f", target_pose.pose.orientation.z);
            target_pose.pose.orientation.w = (double)goal_json["global_pose_target"]["orientation"]["w"];
            trajectory_json["goal"]["global_pose"]["orientation"]["w"] = (double)goal_json["global_pose_target"]["orientation"]["w"];
            ROS_INFO_NAMED("planning_server", "Target orientation w: %f", target_pose.pose.orientation.w);
        } catch(nlohmann::json::exception const& json_ex){
            success = false;
            error_info = "Error in JSON while trying to get global target pose.\n" + *json_ex.what();
            setResult(success, error_info, "");
            as_.setAborted(result_);
            return;
        } catch(std::invalid_argument const& inval_arg_ex){
            success = false;
            error_info = "Invalid value in global target pose. Expected a numerical value.\n" + *inval_arg_ex.what();
            setResult(success, error_info, "");
            as_.setAborted(result_);
            return;
        } catch(std::exception const& ex){
            success = false;
            error_info = "Error while processing global target pose.\n" + *ex.what();
            setResult(success, error_info, "");
            as_.setAborted(result_);
            return;
        }
        try{
            this->tf_buffer_ptr->transform(target_pose, target_pose, "base_link");
        } catch (tf2::TransformException &ex)
        {
            ROS_WARN("Could not perform transform from global --> base_link.\n%s", ex.what());
            success = false;
            error_info = "Could not perform transform from global --> base_link.\n" + *ex.what();
            setResult(success, error_info, "");
            as_.setAborted(result_);
            return;
        }
        planning_component->setGoal(target_pose, "gripper_tcp");
    } else if(goal_json["state_target"].dump() != "{}"){
        ROS_WARN("RECEIVED STATE TARGET!!");
        moveit::core::RobotStatePtr robot_target_state = this->moveit_cpp_ptr->getCurrentState();
        joint_value_buffer.clear();
        for(std::string joint_name : joint_names_from_json){
            try{
                joint_value_buffer.push_back((double)goal_json["state_target"][joint_name]);
                trajectory_json["goal"]["state"][joint_name] = (double)goal_json["state_target"][joint_name];
            } catch(nlohmann::json::exception const& json_ex){
                success = false;
                error_info = "Error in JSON while trying to get joint value target for joint " + joint_name + ".\n" + *json_ex.what();
                setResult(success, error_info, "");
                as_.setAborted(result_);
                return;
            } catch(std::invalid_argument const& inval_arg_ex){
                success = false;
                error_info = "Invalid value in state_target for joint " + joint_name + ". Expected a numerical value.\n" + *inval_arg_ex.what();
                setResult(success, error_info, "");
                as_.setAborted(result_);
                return;
            } catch(std::exception const& ex){
                success = false;
                error_info = "Error while processing joint value target for joint " + joint_name + ".\n" + *ex.what();
                setResult(success, error_info, "");
                as_.setAborted(result_);
                return;
            }
        }
        robot_start_state->setJointGroupPositions(joint_model_group, joint_value_buffer);
        planning_component->setGoal(*robot_target_state);
    } else if (goal_json["module_pose_target"].dump() != "{}"){
        geometry_msgs::PoseStamped target_pose;
        try{
            target_pose.pose.position.x = (double)goal_json["module_pose_target"]["position"]["x"];
            trajectory_json["goal"]["module_pose"]["position"]["x"] = (double)goal_json["module_pose_target"]["position"]["x"];
            ROS_INFO_NAMED("planning_server", "Target position x: %f", target_pose.pose.position.x);
            target_pose.pose.position.y = (double)goal_json["module_pose_target"]["position"]["y"];
            trajectory_json["goal"]["module_pose"]["position"]["y"] = (double)goal_json["module_pose_target"]["position"]["y"];
            ROS_INFO_NAMED("planning_server", "Target position y: %f", target_pose.pose.position.y);
            target_pose.pose.position.z = (double)goal_json["module_pose_target"]["position"]["z"];
            trajectory_json["goal"]["module_pose"]["position"]["z"] = (double)goal_json["module_pose_target"]["position"]["z"];
            ROS_INFO_NAMED("planning_server", "Target position z: %f", target_pose.pose.position.z);
            target_pose.pose.orientation.x = (double)goal_json["module_pose_target"]["orientation"]["x"];
            trajectory_json["goal"]["module_pose"]["orientation"]["x"] = (double)goal_json["module_pose_target"]["orientation"]["x"];
            ROS_INFO_NAMED("planning_server", "Target orientation x: %f", target_pose.pose.orientation.x);
            target_pose.pose.orientation.y = (double)goal_json["module_pose_target"]["orientation"]["y"];
            trajectory_json["goal"]["module_pose"]["orientation"]["y"] = (double)goal_json["module_pose_target"]["orientation"]["y"];
            ROS_INFO_NAMED("planning_server", "Target orientation y: %f", target_pose.pose.orientation.y);
            target_pose.pose.orientation.z = (double)goal_json["module_pose_target"]["orientation"]["z"];
            trajectory_json["goal"]["module_pose"]["orientation"]["z"] = (double)goal_json["module_pose_target"]["orientation"]["z"];
            ROS_INFO_NAMED("planning_server", "Target orientation z: %f", target_pose.pose.orientation.z);
            target_pose.pose.orientation.w = (double)goal_json["module_pose_target"]["orientation"]["w"];
            trajectory_json["goal"]["module_pose"]["orientation"]["w"] = (double)goal_json["module_pose_target"]["orientation"]["w"];
            ROS_INFO_NAMED("planning_server", "Target orientation w: %f", target_pose.pose.orientation.w);
        } catch(nlohmann::json::exception const& json_ex){
            success = false;
            error_info = "Error in JSON while trying to get global target pose.\n" + *json_ex.what();
            setResult(success, error_info, "");
            as_.setAborted(result_);
            return;
        } catch(std::invalid_argument const& inval_arg_ex){
            success = false;
            error_info = "Invalid value in global target pose. Expected a numerical value.\n" + *inval_arg_ex.what();
            setResult(success, error_info, "");
            as_.setAborted(result_);
            return;
        } catch(std::exception const& ex){
            success = false;
            error_info = "Error while processing global target pose.\n" + *ex.what();
            setResult(success, error_info, "");
            as_.setAborted(result_);
            return;
        }
        try{
            std::unique_ptr<std::string> value_string = std::make_unique<std::string>("");
            std::unique_ptr<double> value = std::make_unique<double>(0.0);
            *value_string = goal_json["module_pose_target"]["module_id"].dump();
            value_string->erase(remove(value_string->begin(), value_string->end(), '\"'), value_string->end());
            target_pose.header.frame_id = *value_string;
            trajectory_json["goal"]["module_pose"]["module_id"] = *value_string;
            trajectory_json["goal"]["module_pose"]["grasp_id"] = "some_grasp";
            this->tf_buffer_ptr->transform(target_pose, target_pose, "base_link");
        } catch (tf2::TransformException &ex)
        {
            ROS_WARN("Could not perform transform from global --> %s.\n%s", goal_json["module_pose_target"]["module_id"].dump(), ex.what());
            success = false;
            error_info = "Could not perform transform from global --> " + goal_json["module_pose_target"]["module_id"].dump() + ".\n" + *ex.what();
            setResult(success, error_info, "");
            as_.setAborted(result_);
            return;
        }
        planning_component->setGoal(target_pose, "gripper_tcp");
        
    }
    moveit_cpp::PlanningComponent::MultiPipelinePlanRequestParameters multi_pipeline_plan_request{*(this->nh_ptr), {}};
    //{ "ompl_bitrrt", "ompl_rrtc", "ompl_biest" }
    //{ "ompl_rrt_star", "ompl_fmt", "ompl_prm_star" }
    moveit_cpp::PlanningComponent::PlanRequestParameters rrt_star_parameters;
    rrt_star_parameters.planning_attempts = 5;
    rrt_star_parameters.planning_pipeline = "ompl_rrt_star";
    rrt_star_parameters.planner_id = "RRTstar";
    rrt_star_parameters.max_velocity_scaling_factor = 1.0;
    rrt_star_parameters.max_acceleration_scaling_factor = 1.0;
    rrt_star_parameters.planning_time = 5.0;
    moveit_cpp::PlanningComponent::PlanRequestParameters prm_star_parameters;
    prm_star_parameters.planning_attempts = 5;
    prm_star_parameters.planning_pipeline = "ompl_prm_star";
    prm_star_parameters.planner_id = "PRMstar";
    prm_star_parameters.max_velocity_scaling_factor = 1.0;
    prm_star_parameters.max_acceleration_scaling_factor = 1.0;
    prm_star_parameters.planning_time = 5.0;
    moveit_cpp::PlanningComponent::PlanRequestParameters fmt_paramters;
    fmt_paramters.planning_attempts = 5;
    fmt_paramters.planning_pipeline = "ompl_fmt";
    fmt_paramters.planner_id = "FMT";
    fmt_paramters.max_velocity_scaling_factor = 1.0;
    fmt_paramters.max_acceleration_scaling_factor = 1.0;
    fmt_paramters.planning_time = 5.0;
    moveit_cpp::PlanningComponent::PlanRequestParameters rrtc_parameters;
    rrtc_parameters.planning_attempts = 5;
    rrtc_parameters.planning_pipeline = "ompl_rrtc";
    rrtc_parameters.planner_id = "RRTConnect";
    rrtc_parameters.max_velocity_scaling_factor = 1.0;
    rrtc_parameters.max_acceleration_scaling_factor = 1.0;
    rrtc_parameters.planning_time = 1.0;
    moveit_cpp::PlanningComponent::PlanRequestParameters bitrrt_parameters;
    bitrrt_parameters.planning_attempts = 5;
    bitrrt_parameters.planning_pipeline = "ompl_bitrrt";
    bitrrt_parameters.planner_id = "BiTRRT";
    bitrrt_parameters.max_velocity_scaling_factor = 1.0;
    bitrrt_parameters.max_acceleration_scaling_factor = 1.0;
    bitrrt_parameters.planning_time = 1.0;
    moveit_cpp::PlanningComponent::PlanRequestParameters biest_parameters;
    biest_parameters.planning_attempts = 5;
    biest_parameters.planning_pipeline = "ompl_biest";
    biest_parameters.planner_id = "BiEST";
    biest_parameters.max_velocity_scaling_factor = 1.0;
    biest_parameters.max_acceleration_scaling_factor = 1.0;
    biest_parameters.planning_time = 1.0;
    multi_pipeline_plan_request.multi_plan_request_parameters.clear();
    multi_pipeline_plan_request.multi_plan_request_parameters.reserve(3);
    //multi_pipeline_plan_request.multi_plan_request_parameters.push_back(bitrrt_parameters);
    //multi_pipeline_plan_request.multi_plan_request_parameters.push_back(rrtc_parameters);
    //multi_pipeline_plan_request.multi_plan_request_parameters.push_back(biest_parameters);
    multi_pipeline_plan_request.multi_plan_request_parameters.push_back(rrt_star_parameters);
    multi_pipeline_plan_request.multi_plan_request_parameters.push_back(prm_star_parameters);
    multi_pipeline_plan_request.multi_plan_request_parameters.push_back(fmt_paramters);
    planning_interface::MotionPlanResponse mpr = planning_component->plan(multi_pipeline_plan_request, &getShortestWeightedSolution, &stoppingCriterion); //, &stoppingCriterion
    success = (mpr.error_code_ == moveit::core::MoveItErrorCode::SUCCESS);



    if(success){
        //robot_trajectory::RobotTrajectory robot_traj(this->moveit_cpp_ptr->getRobotModel(), "arm");
        //robot_traj.setRobotTrajectoryMsg(*robot_start_state, mpr.trajectory_);
        //planning_component->execute();
        //ROS_INFO_NAMED("tutorial", "TEST");
        //ros::Publisher display_publisher = this->nh_ptr->advertise<moveit_msgs::DisplayTrajectory>("/move_group/display_planned_path", 1, true); //vis
        //moveit_msgs::DisplayTrajectory display_trajectory; //vis
        robot_trajectory::RobotTrajectoryPtr robot_traj = mpr.trajectory_;
        moveit_msgs::RobotTrajectory robot_traj_msg;
        //moveit_msgs::RobotState robot_state_msg; //vis
        robot_traj->getRobotTrajectoryMsg(robot_traj_msg);
        //moveit::core::robotStateToRobotStateMsg (*robot_start_state, robot_state_msg, true); //vis
        
        //display_trajectory.trajectory_start = robot_state_msg; //vis
        //display_trajectory.trajectory.push_back(robot_traj_msg); //vis
        //sleep(2.0); //vis 
        //display_publisher.publish(display_trajectory); //vis
        
        for (std::size_t i = 0; i < joint_names_from_json.size(); ++i) {
            trajectory_json["start"]["state"][joint_names_from_json[i]] = (*(this->start_joint_values))[i];
            trajectory_json["joint_names"] += joint_names_from_json[i];
        }
        std::vector<double>* joint_positions = new std::vector<double>();
        for(std::size_t i = 0; i < robot_traj->getWayPointCount(); ++i){
            joint_positions->clear();
            robot_traj->getWayPoint(i).copyJointGroupPositions(joint_model_group, *joint_positions);
            nlohmann::json waypoint_json;
            for(double joint_value : *joint_positions){
                waypoint_json["positions"] += joint_value;
            }
            trajectory_json["waypoints"] += waypoint_json;
        }

        std::ofstream results_file;
        std::string results_file_name =  "trajectories/" + traj_id.str() + ".json";
        //std::string results_file_name = "test.csv";
        results_file.open(results_file_name, std::ios::out | std::ios::trunc);
        results_file << trajectory_json.dump(4);
        results_file.close();

        //sleep(5.0);
        //ROS_INFO_NAMED("tutorial", "TEST2");
        const robot_state::RobotState& target_state = robot_traj->getWayPoint(robot_traj->getWayPointCount() - 1);
        this->start_joint_values->clear();
        target_state.copyJointGroupPositions(joint_model_group, *(this->start_joint_values));
        setResult(true, "", results_file_name);
        as_.setSucceeded(result_);
        return;
    }
    setResult(false, "Could not find feasible path.", "");
    as_.setAborted(result_);
}
