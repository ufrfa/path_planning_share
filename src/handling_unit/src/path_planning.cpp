#include <ros/ros.h>
#include <string>
#include "geometric_shapes/shapes.h"
#include "geometric_shapes/mesh_operations.h"
#include "geometric_shapes/shape_operations.h"
#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <geometric_shapes/shape_operations.h>
#include <moveit/moveit_cpp/moveit_cpp.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <moveit_visual_tools/moveit_visual_tools.h>
#include <eigen_conversions/eigen_msg.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <tf_conversions/tf_eigen.h>
#include <actionlib/server/simple_action_server.h>
#include <handling_unit/Planning_requestAction.h>
#include "handling_unit/init_planning_action_server.hpp"
#include "handling_unit/planning_request_action_server.hpp"
#include <stdlib.h>
#include <moveit/moveit_cpp/moveit_cpp.h>
#include <moveit/planning_scene_monitor/planning_scene_monitor.h>



int main(int argc, char** argv)
{
    /*
    std::string env_var_name = "ROS_MASTER_URI";
    std::string env_var_value = "http://localhost:11312/"; //http://localhost:11311
    setenv(env_var_name.c_str(), env_var_value.c_str(), 1);
    */
    ros::init(argc, argv, "path_planning");
    ros::NodeHandle node_handle("/path_planning");
    ros::AsyncSpinner spinner(2);
    spinner.start();
    static const std::string PLANNING_GROUP = "arm";
    ros::Duration(1.0).sleep();
    auto moveit_cpp_ptr = std::make_shared<moveit_cpp::MoveItCpp>(node_handle);
    auto planning_component = std::make_shared<moveit_cpp::PlanningComponent>("arm", moveit_cpp_ptr);
    moveit::planning_interface::MoveGroupInterface move_group_interface(PLANNING_GROUP);
    auto psm = moveit_cpp_ptr->getPlanningSceneMonitorNonConst();
    psm->startSceneMonitor();
    psm->startWorldGeometryMonitor();
    psm->startStateMonitor();
    psm->startPublishingPlanningScene(planning_scene_monitor::PlanningSceneMonitor::SceneUpdateType::UPDATE_SCENE);
    psm->providePlanningSceneService();
    moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
    InitPlanningActionServer init_planning_as("init_planning_action_server", move_group_interface.getPlanningFrame(), &planning_scene_interface);
    PlanningRequestActionServer planning_request_as("planning_request_action_server", moveit_cpp_ptr, &node_handle);
    ros::waitForShutdown();
    return 0;
}