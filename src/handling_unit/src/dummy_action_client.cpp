#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <handling_unit/Init_planningAction.h>
#include <handling_unit/Planning_requestAction.h>
#include <nlohmann/json.hpp>
#include <chrono>

int main (int argc, char **argv)
{
  ros::init(argc, argv, "dummy_action_client");

  // create the action client
  // true causes the client to spin its own thread
  actionlib::SimpleActionClient<handling_unit::Init_planningAction> init_planning_ac("init_planning_action_server", true);
  actionlib::SimpleActionClient<handling_unit::Planning_requestAction> planning_request_ac("planning_request_action_server", true);

  ROS_INFO("Waiting for action server to start.");
  // wait for the action server to start
  init_planning_ac.waitForServer(); //will wait for infinite time
  planning_request_ac.waitForServer();

  ROS_INFO("Action server started, sending goal.");
  // send a goal to the action
  nlohmann::json init_goal_json = nlohmann::json::parse(R"(
  {
    "table_config": [
        {
           "id": 1,
            "position": {
                "x": -1,
                "y": 0
            }
        }
    ],
    "modules": [
            {
                "module_id": "Magnetentnahmeeinheit",
                "3d_mesh": "/home/loris/share_ws/src/descriptions/units/magneteinheit.dae",
                "pose": {
                    "plate": {
                        "x": 1,
                        "y": 1
                    },
                    "orientation": 0 
                }
            },
            {
                "module_id": "storage_A",
                "3d_mesh": "",
                "pose": {
                    "plate": {
                        "x": 1,
                        "y": 0
                    },
                    "orientation": 0 
                }
            },
            {
                "module_id": "Ausspresseinheit",
                "3d_mesh": "/home/loris/share_ws/src/descriptions/units/auspresseinheit.dae",
                "pose": {
                    "plate": {
                        "x": 0,
                        "y": 1
                    },
                    "orientation": 0
                }
            }
        ],
    "robot": {
        "position": {
            "x": -0.135,
            "y": -0.135
        }
    }
    
  }
    )");





  handling_unit::Init_planningGoal init_goal;
  init_goal.configuration = init_goal_json.dump();
  auto start = std::chrono::high_resolution_clock::now();
  init_planning_ac.sendGoal(init_goal);

  //wait for the action to return
  bool finished_before_timeout = init_planning_ac.waitForResult(ros::Duration(30.0));

  if (finished_before_timeout)
  {
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    actionlib::SimpleClientGoalState state = init_planning_ac.getState();
    ROS_INFO("Action finished: %s in %li milliseconds",state.toString().c_str(), duration.count());
    handling_unit::Init_planningResult result = *(init_planning_ac.getResult());
    std::string result_string = result.result;
    ROS_INFO("Action result: %s", result_string.c_str());
  }
  else
    ROS_INFO("Action did not finish before the time out.");

  ros::Duration(8).sleep();


  //GW1 --> GW2
  nlohmann::json planning_goal_json = nlohmann::json::parse(R"(
    {
      "start_state": {
        "shoulder_pan_joint": 2.1765336990356445,
        "shoulder_lift_joint": -1.028496579532959,
        "elbow_joint": -2.580817222595215,
        "wrist_1_joint": -2.690824171105856,
        "wrist_2_joint": -0.9566710630999964,
        "wrist_3_joint": 0.02308511734008789
      },
      "global_pose_target": {},
      "state_target": {
        "shoulder_pan_joint": 2.1197662353515625,
        "shoulder_lift_joint": -1.9341346226134242,
        "elbow_joint": -2.001896858215332,
        "wrist_1_joint": -2.3011557064452113,
        "wrist_2_joint": -2.5758984724627894,
        "wrist_3_joint": 0.027216434478759766
      },
      "module_pose_target": {},
      "gripper_open": true,
      "object_gripped": true
    }
    )");


    handling_unit::Planning_requestGoal planning_goal;
    planning_goal.planning_request = planning_goal_json.dump();
    ROS_INFO("Send Goal");
    start = std::chrono::high_resolution_clock::now();
    planning_request_ac.sendGoal(planning_goal);

    finished_before_timeout = planning_request_ac.waitForResult(ros::Duration(30.0));

    if (finished_before_timeout)
    {
      auto stop = std::chrono::high_resolution_clock::now();
      auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
      actionlib::SimpleClientGoalState state = planning_request_ac.getState();
      ROS_INFO("Action finished: %s in %li milliseconds",state.toString().c_str(), duration.count());
      handling_unit::Planning_requestResult result = *(planning_request_ac.getResult());
      std::string result_string = result.result;
      ROS_INFO("Action result: %s", result_string.c_str());
    }
    else
      ROS_INFO("Action did not finish before the time out.");


    ros::Duration(8).sleep();


  //GW2 --> GW3
  planning_goal_json = nlohmann::json::parse(R"(
    {
      "start_state": {
        "shoulder_pan_joint": 2.1197662353515625,
        "shoulder_lift_joint": -1.9341346226134242,
        "elbow_joint": -2.001896858215332,
        "wrist_1_joint": -2.3011557064452113,
        "wrist_2_joint": -2.5758984724627894,
        "wrist_3_joint": 0.027216434478759766
      },
      "global_pose_target": {},
      "state_target": {
        "shoulder_pan_joint": 3.3609230518341064,
        "shoulder_lift_joint": -0.9208529752543946,
        "elbow_joint": -2.562106132507324,
        "wrist_1_joint": -2.7842632732787074,
        "wrist_2_joint": -1.3456957975970667,
        "wrist_3_joint": 3.1387887001037598
      },
      "module_pose_target": {},
      "gripper_open": false,
      "object_gripped": false
    }
    )");


    //[3.3609230518341064, -0.9208529752543946, -2.562106132507324, -2.7842632732787074, -1.3456957975970667, 3.1387887001037598]


    
    planning_goal.planning_request = planning_goal_json.dump();
    ROS_INFO("Send Goal");
    start = std::chrono::high_resolution_clock::now();
    planning_request_ac.sendGoal(planning_goal);

    finished_before_timeout = planning_request_ac.waitForResult(ros::Duration(30.0));

    if (finished_before_timeout)
    {
      auto stop = std::chrono::high_resolution_clock::now();
      auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
      actionlib::SimpleClientGoalState state = planning_request_ac.getState();
      ROS_INFO("Action finished: %s in %li milliseconds",state.toString().c_str(), duration.count());
      handling_unit::Planning_requestResult result = *(planning_request_ac.getResult());
      std::string result_string = result.result;
      ROS_INFO("Action result: %s", result_string.c_str());
    }
    else
      ROS_INFO("Action did not finish before the time out.");


    
    ros::Duration(8).sleep();



  //GW3 --> GW1
  planning_goal_json = nlohmann::json::parse(R"(
    {
      "start_state": {
        "shoulder_pan_joint": 3.3609230518341064,
        "shoulder_lift_joint": -0.9208529752543946,
        "elbow_joint": -2.562106132507324,
        "wrist_1_joint": -2.7842632732787074,
        "wrist_2_joint": -1.3456957975970667,
        "wrist_3_joint": 3.1387887001037598
      },
      "global_pose_target": {},
      "state_target": {
        "shoulder_pan_joint": 2.1765336990356445,
        "shoulder_lift_joint": -1.028496579532959,
        "elbow_joint": -2.580817222595215,
        "wrist_1_joint": -2.690824171105856,
        "wrist_2_joint": -0.9566710630999964,
        "wrist_3_joint": 0.02308511734008789
      },
      "module_pose_target": {},
      "gripper_open": true,
      "object_gripped": false
    }
    )");


    //[3.3609230518341064, -0.9208529752543946, -2.562106132507324, -2.7842632732787074, -1.3456957975970667, 3.1387887001037598]


    
    planning_goal.planning_request = planning_goal_json.dump();
    ROS_INFO("Send Goal");
    start = std::chrono::high_resolution_clock::now();
    planning_request_ac.sendGoal(planning_goal);

    finished_before_timeout = planning_request_ac.waitForResult(ros::Duration(30.0));

    if (finished_before_timeout)
    {
      auto stop = std::chrono::high_resolution_clock::now();
      auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
      actionlib::SimpleClientGoalState state = planning_request_ac.getState();
      ROS_INFO("Action finished: %s in %li milliseconds",state.toString().c_str(), duration.count());
      handling_unit::Planning_requestResult result = *(planning_request_ac.getResult());
      std::string result_string = result.result;
      ROS_INFO("Action result: %s", result_string.c_str());
    }
    else
      ROS_INFO("Action did not finish before the time out.");

  

  //exit
  return 0;
}