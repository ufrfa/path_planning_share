## Attention
**Recently a new parameter "gripper_open" was added to planning requests. This parameter is a boolean and is expected by the planning action service. It can be added to the json of any planning request like so:**

    "gripper_open": true

**or as well:**

    "gripper_open": false

**When `"gripper_open": false` the gripper will be considered as closed for this planning request. When `"gripper_open": true` the gripper will be considered as about 3cm opened.**

**Additionally, another parameter "object_gripped" was added to the planning request. This parameter is a boolean and is expected by the planning action service. It can be added to the json of any planning request like so:**

    "object_gripped": true

**or as well:**

    "object_gripped": false 

**When `"object_gripped": true` a typical object will be considered as attached to the gripper for this planning request. Please note that the gripper can not be closed and gripping an object at the same time. So the combination `"gripper_open": false` and `"object_gripped": true` is forbidden and will lead to an error.**

**As these parameters are expected, planning requests that don't include these parameters won't work anymore!**

**Recently the planning target link was changed from "tool0" (which is located at the end of the last link of the robot) to "gripper_tcp" which is the TCP of the gripper. For planning, please specify targets w.r.t. gripper_tcp. Targets that were specified w.r.t. tool0 will not yield the same results anymore!**

# Path Plannning

Build the workspace with: catkin build

## Path Planning

Run Path Planning Module:
- source devel/setup.bash
- roslaunch handling_unit_moveit_config path_planning.launch

Path planning is devided into two ROS Actions: Init_Planning and Planning_Request. The Action Init_Planning accepts information about the robots environment and creates a planning scene for further path planning. The Action Planning_Requests accepts a start state (joint angle values) for the robot and a target pose. It requires a pointer to the same Move Group Interface Object that Init_Planning used to configure the planning scene. The Planning_Request is the part that actually does the path planning and returns the planned trajecotry. Path_Planning connects the two actions and defines the ROS node that runs them.

Planned trajecotries are saved into the .ros folder in your home directory. You can choose a different folder within .ros by changing the results_file_name in line 459 in src/handling_unit/src/planning_request_action_server.cpp

To just test the path planning node you can run the dummy_action_client. This node just sends planning requests to the path_planning node and prints the response.
Because the dummy_action_client also sends information about the environment, it needs the path to the 3D-meshes of the machines etc. Please go to src/handling_unit/src/dummy_action_client.cpp and change all occurrences of /home/loris/test_ws/ to the path of YOUR workspace.
I know this should ideally be done more cleanly but this is how it works for now.

Run Dummy Action Client:
- source devel/setup.bash
- roslaunch handling_unit dummy_action_client.launch

### Display a trajectory from a json file
You can display a trajectory from a trajectory json file by running the display_trajectory node.
As a prerequisite, you need to again change the path to the 3D-meshes in the code.
Go to src/handling_unit/src/display_trajectory.cpp and change all occurrences of /home/loris/test_ws/ to the path to YOUR workspace.
You can also use the env_builder object to build your own environment.

Start new Terminal and navigate to your workspace
- source devel/setup.bash
- roslaunch handling_unit_moveit_config demo.launch

Start new Terminal and navigate to your workspace
- roslaunch handling_unit display_trajectory.launch

Wait until all the environment is loaded

Start new Terminal and Execute:
- rostopic pub trajectories std_msgs/String '{data: 'path/to/your/trajecotry/file.json'}'

For example: rostopic pub trajectories std_msgs/String '{data: 'trajectories/some_trajectory_id.json'}'
to display:  some_trajectory_id.json in .ros/trajectories
